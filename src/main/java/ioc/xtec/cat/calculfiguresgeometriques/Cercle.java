/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author Miriam Flores Diéguez
 */
public class Cercle implements FiguraGeometrica {

    private final double radi;
    private final double pi = 3.141592;

    public Cercle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del radi del cercle: ");
        this.radi = scanner.nextDouble();
    }

    @Override
    public double calcularArea() {
        return pi * radi * radi;
    }

    @Override
    public double calcularPerimetre() {
        return pi * radi * 2;
    }

}
