/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author Miriam Flores Diéguez
 */
public class Rectangle implements FiguraGeometrica {

    private final double costat1;
    private final double costat2;

    public Rectangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud d'un dels costats del rectangle: ");
        this.costat1 = scanner.nextDouble();
        System.out.println("Introduïu la longitud de l'altre costat del rectangle: ");
        this.costat2 = scanner.nextDouble();
    }

    @Override
    public double calcularArea() {
        return costat1 * costat2;
    }

    @Override
    public double calcularPerimetre() {
        return costat1 * 2 + costat2 * 2;
    }

}
